<?php

//set path to log files
$log_path = '/var/www/bugtracker.local/feedbackfolder';

header('Content-Type: text/html; charset=utf-8');
error_reporting(E_ALL);
ini_set('display_errors', 1);

require_once "lib/Data.php";
require_once "lib/Routing.php";

new Routing();
new Data();

Data::setParams($log_path, Routing::getRequestType());
Routing::getTemplate(Data::get());