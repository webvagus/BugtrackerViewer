$(document).ready(function () {

    var container = document.getElementById('crashreports');

    var advancedData = $.parseJSON($('#data').text());

    if(typeof advancedData != 'object') {
        alert('Wrong data from server: ' + $('#data').text());
        return false;
    }

    var hot = new Handsontable(container, {
        data: advancedData,
        height: 600,
        colHeaders: ["Date", "Version", "Email", "Comment", "Console", "Crashes", "Prefs", "Sys"],
        rowHeaders: true,
        stretchH: 'all',
        columnSorting: true,
        contextMenu: true,
        className: "htCenter htMiddle",
        readOnly: true,
        columns: [
                    {data: 0, type: 'text'},
                    {data: 1, type: 'text'},
                    {data: 2, type: 'text'},
                    {data: 3, type: 'text'},
                    {data: 4, renderer: "html"},
                    {data: 5, renderer: "html"},
                    {data: 6, renderer: "html"},
                    {data: 7, renderer: "html"}
                ]
    });
});
