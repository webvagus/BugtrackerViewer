<?php

class Data
{
    protected static $path;
    protected static $request_type;
    protected static $count_on_page = 50;
    protected static $start_from = 2;

    static public function setParams($main_path, $request_type)
    {
        self::$path = $main_path;
        self::$request_type = $request_type;

        if (!is_dir(self::$path)) {
            return self::getJson('Log directory does not exist');
        }
    }

    static public function get()
    {
        if (self::$request_type['type'] == 'all') {
            $data = self::getAllData();
        }

        if (self::$request_type['type'] == 'file') {
            $data = self::getFileData();
        }
        return $data;
    }

    static public function cleanAndSort($dirs)
    {
        unset($dirs[0]);
        unset($dirs[1]);

        $dirs = array_reverse($dirs);

        return $dirs;
    }

    static private function slice_for_page($dirs)
    {
        $page = self::$request_type['page'];

        $dirs = self::cleanAndSort($dirs);

        if ($page == 'all') {
            return $dirs;
        }

        //get first position for pagination
        if ($page == 1) {
            $from = self::$start_from;
        } else {
            $from = (self::$count_on_page * ($page-1)) + self::$start_from;
        }

        //take only the necessary data for the page
        return array_slice($dirs, $from, self::$count_on_page);
    }

    static private function getAllData()
    {
        $main_path = self::$path;

        $dirs = scandir($main_path);

        $dirs = self::slice_for_page($dirs);

        $content = array();

        foreach ($dirs as $key => $dir){

            if (is_dir($main_path . '/' . $dir)) {
                if ($dh = scandir($main_path . '/' . $dir)) {

                    $version = (array_key_exists(11, $dh) && is_file($main_path . '/' . $dir . '/' . $dh[11]))? escapeshellcmd(file_get_contents($main_path . '/' . $dir . '/' . $dh[11])) : '';
                    $email = (array_key_exists(5, $dh) && is_file($main_path . '/' . $dir . '/' . $dh[5]))? escapeshellcmd(file_get_contents($main_path . '/' . $dir . '/' . $dh[5])) : '';
                    $comment = (array_key_exists(2, $dh) && is_file($main_path . '/' . $dir . '/' . $dh[2]))? escapeshellcmd(file_get_contents($main_path . '/' . $dir . '/' . $dh[2])) : '';
                    $date = substr($dir, 0, 19);
                    $date = explode('T', $date);
                    $date = ($date)? $date[0] . ' | ' . $date[1] : '';

                    $console     = (array_key_exists(3, $dh))? $dh[3] : 'empty';
                    $crashes     = (array_key_exists(4, $dh))? $dh[4] : 'empty';
                    $preferences = (array_key_exists(7, $dh))? $dh[7] : 'empty';
                    $system      = (array_key_exists(9, $dh))? $dh[9] : 'empty';

                    $console_link     = "<a href='index.php?type=file&dir={$dir}&file={$console}' target=_blank>Open</a>";
                    $crashes_link     = "<a href='index.php?type=file&dir={$dir}&file={$crashes}' target=_blank>Open</a>";
                    $preferences_link = "<a href='index.php?type=file&dir={$dir}&file={$preferences}' target=_blank>Open</a>";
                    $system_link      = "<a href='index.php?type=file&dir={$dir}&file={$system}' target=_blank>Open</a>";

                    $content[] = array(
                                    $date,
                                    $version,
                                    $email,
                                    $comment,
                                    $console_link,
                                    $crashes_link,
                                    $preferences_link,
                                    $system_link
                                );
                }
            }
        }

        $data = array();

        $page = self::$request_type['page'];

        //set page for template pagination
        if ($page == 1 || $page == 'all') {
            $data['page_from'] = 1;
            $data['page_to'] = 2;
        } else {
            $data['page_from'] = $page - 1;
            $data['page_to'] = $page + 1;
        }

        $data['content'] = htmlspecialchars(json_encode($content));

        return $data;
    }

    static private function getFileData()
    {
        $file_path = self::$path . '/' . self::$request_type['fileDir'] . '/' . self::$request_type['fileName'];

        if (!file_exists($file_path)) {
            return 'File does not exist';
        }

        $f = escapeshellcmd(file_get_contents($file_path));

        $data = '';

        $content = explode("\n", $f);

        foreach($content as $string) {
            $data .= htmlspecialchars($string) . '<br>';
        }

        return $data;
    }
}