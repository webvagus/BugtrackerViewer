<?php

class Routing
{
    static private $param = array();

    public function __construct()
    {
        if (empty($_GET['page'])) {
            self::$param['page'] = '1';
        } elseif ($_GET['page'] == 'all') {
            self::$param['page'] = 'all';
        } else {
            self::$param['page'] = (int)$_GET['page'];
        }

        if (empty($_GET['type'])) {
            $_GET['type'] = 'all';
        }

        if ($_GET['type'] == 'all') {
            self::$param['type'] = 'all';
        } elseif ($_GET['type'] == 'file' && $_GET['dir'] && $_GET['file']) {
            self::$param['type'] = 'file';
            self::$param['fileName'] = htmlspecialchars(escapeshellcmd($_GET['file']));
            self::$param['fileDir'] = htmlspecialchars(escapeshellcmd($_GET['dir']));
        } else {
            self::$param['type'] = 'all';
        }
    }

    static public function getRequestType()
    {
        return self::$param;
    }

    static public function getTemplate($data_template)
    {
        if (self::$param['type'] == 'all') {
            $output = require "template/main-table.html";
        } else {
            $output = require "template/file-content.html";
        }
        return $output;
    }
}